<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.UUID"%>
<%@page import="java.security.interfaces.RSAPublicKey"%>
<%@page import="org.apache.commons.lang.ArrayUtils"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page
	import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.zero.common.util.SpringUtils"%>
<%@page import="com.zero.common.service.RSAService"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
    	String base = request.getContextPath();
			String captchaId = UUID.randomUUID().toString();
			ApplicationContext applicationContext = SpringUtils.getApplicationContext();
			if (applicationContext != null) {
%>
<shiro:authenticated>
	<%
	    response.sendRedirect(request.getContextPath() + "/admin/common/main.jhtml");
	%>
</shiro:authenticated>
<%
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
    RSAService rsaService = SpringUtils.getBean("rsaServiceImpl", RSAService.class);
			RSAPublicKey publicKey = rsaService.generateKey(request);
			String modulus = Base64.encodeBase64String(publicKey.getModulus().toByteArray());
			String exponent = Base64.encodeBase64String(publicKey.getPublicExponent().toByteArray());

			String message = null;
			String loginFailure = (String) request
					.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
			if (loginFailure != null) {
				if (loginFailure.equals("org.apache.shiro.authc.pam.UnsupportedTokenException")) {
					message = "admin.captcha.invalid";
				} else if (loginFailure.equals("org.apache.shiro.authc.UnknownAccountException")) {
					message = "admin.login.unknownAccount";
				} else if (loginFailure.equals("org.apache.shiro.authc.DisabledAccountException")) {
					message = "admin.login.disabledAccount";
				} else if (loginFailure.equals("org.apache.shiro.authc.LockedAccountException")) {
					message = "admin.login.lockedAccount";
				} else if (loginFailure.equals("org.apache.shiro.authc.IncorrectCredentialsException")) {
					message = "admin.login.accountLockCount";
				} else if (loginFailure.equals("org.apache.shiro.authc.AuthenticationException")) {
					message = "admin.login.authentication";
				}
			}
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SaleNZ</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet"
	href="${servletContext.contextPath }/resources/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${servletContext.contextPath }/resources/dist/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet"
	href="${servletContext.contextPath }/resources/plugins/iCheck/square/blue.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<b>Admin</b>SaleNZ
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<form action="<%=base%>/admin/login.jsp" method="post" id="loginForm">
				<input type="hidden" id="enPassword" name="enPassword" />
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="username" id="username" placeholder="Username">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label> <input type="checkbox" id="remenberMe">
								Remember Me
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
							In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>

		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 2.2.0 -->
	<script
		src="${servletContext.contextPath }/resources/plugins/jQuery/jQuery-2.2.0.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script
		src="${servletContext.contextPath }/resources/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script
		src="${servletContext.contextPath }/resources/plugins/iCheck/icheck.min.js"></script>
	<script type="text/javascript" src="<%=base%>/resources/admin/js/jsbn.js"></script>
	<script type="text/javascript" src="<%=base%>/resources/admin/js/prng4.js"></script>
	<script type="text/javascript" src="<%=base%>/resources/admin/js/rng.js"></script>
	<script type="text/javascript" src="<%=base%>/resources/admin/js/rsa.js"></script>
	<script type="text/javascript" src="<%=base%>/resources/admin/js/base64.js"></script>
<script>
  $().ready( function() {
		
		var $loginForm = $("#loginForm");
		var $enPassword = $("#enPassword");
		var $username = $("#username");
		var $password = $("#password");
		var $isRememberUsername = $("#remenberMe");
		
		$('input').iCheck({
		      checkboxClass: 'icheckbox_square-blue',
		      radioClass: 'iradio_square-blue',
		      increaseArea: '20%' // optional
		    });
		
		// 记住用户名
		/* if(getCookie("adminUsername") != null) {
			$isRememberUsername.prop("checked", true);
			$username.val(getCookie("adminUsername"));
			$password.focus();
		} else {
			$isRememberUsername.prop("checked", false);
			$username.focus();
		} */
		
		// 表单验证、记住用户名
		$loginForm.submit( function() {
			if ($username.val() == "") {
				$.message("warn", "<%=SpringUtils.getMessage("admin.login.usernameRequired")%>");
				return false;
			}
			if ($password.val() == "") {
				$.message("warn", "<%=SpringUtils.getMessage("admin.login.passwordRequired")%>");
				return false;
			}
			/* if ($isRememberUsername.prop("checked")) {
				addCookie("adminUsername", $username.val(), {expires: 7 * 24 * 60 * 60});
			} else {
				removeCookie("adminUsername");
			} */
			
			var rsaKey = new RSAKey();
			rsaKey.setPublic(b64tohex("<%=modulus%>"), b64tohex("<%=exponent%>"));
			var enPassword = hex2b64(rsaKey.encrypt($password.val()));
			$enPassword.val(enPassword);
			});
	<%if (message != null) {%>
		//
		$(".login-box-msg").html("<%=message%>");
	<%}%>
		});
	</script>
</body>
</html>
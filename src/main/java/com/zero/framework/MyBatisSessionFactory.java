package com.zero.framework;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

/** 
 * Batis SqlSession 工厂获取类
 * @author ytzhang
 */
public class MyBatisSessionFactory {
	
	static Logger logger = Logger.getLogger(MyBatisSessionFactory.class);
	
	static Map<String, SqlSessionFactory> es = new HashMap<String, SqlSessionFactory>();

	/** 
	 * 获取默认数据库  SqlSession
	 */
	static public SqlSession getSqlSession() {
		return getSqlSession("default");
	}
	
	/** 
	 * 获取  SqlSession
	 * @param eid 数据库id
	 */
	static public SqlSession getSqlSession(String eid) {
		SqlSessionFactory ssf = getSqlSessionFactory(eid);
		return ssf.openSession();
	}
	
	static private synchronized SqlSessionFactory getSqlSessionFactory(String eid) {
		if(es.containsKey(eid))
			return es.get(eid);
		
		Properties prop = new Properties();
		InputStream inStream = null;
		try {
			inStream = Resources.getResourceAsStream("mybatis-config.properties");
			prop.load(inStream);
		} catch (IOException ioe) {
			logger.error("load db properties file error", ioe);
		} finally {
			if(inStream != null) try { inStream.close(); } catch (IOException ioe) {}
		}

		String resource = "mybatis-config.xml";
		InputStream inputStream = null;
		SqlSessionFactory sqlSessionFactory = null;
		try {
			inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder()
				.build(inputStream, eid, prop);
			es.put(eid, sqlSessionFactory);
		} catch (IOException ioe) {
			logger.error("SqlSessionFactory build error", ioe);
		} finally {
			if(inputStream != null) try { inputStream.close(); } catch (IOException ioe) {ioe.printStackTrace();}
		}
		
		
		return es.get(eid);
	}
	
}
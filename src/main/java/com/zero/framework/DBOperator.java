package com.zero.framework;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

/** 数据库操作者 */
public class DBOperator {
	
	static Logger logger = Logger.getLogger(DBOperator.class);
	
	private static Properties prop = new Properties();
	static {
		InputStream inStream = null;
		try {
			inStream = DBOperator.class.getClassLoader().getResourceAsStream("mybatis-config.properties");
			prop.load(inStream);
		} catch (IOException ioe) {
			logger.error("load db properties file error", ioe);
		} finally {
			if(inStream != null) try { inStream.close(); } catch (IOException ioe) {}
		}
	}

	public Connection getConnection() {
		return getConnection(null);
	}
	
	/**
	 * 取得数据库连接
	 * @return
	 */
	public Connection getConnection(String dbname) {
		if(conn != null) {
    		Statement stmt = null;
    		try {
    			stmt = conn.createStatement();
    			stmt.executeQuery("select 1");
    			return conn;
    		} catch (Exception e) {
    			if(conn != null) try { conn.close(); conn = null; } catch (Exception e1) {}
    		} finally {
    			if(stmt != null) try { stmt.close(); } catch (Exception e2) {}
    		}
		}
		
		Connection c = null;
		try {
			if(dbname == null) {
				Class.forName(prop.getProperty("driver"));
				c = DriverManager.getConnection(
						prop.getProperty("url"), 
						prop.getProperty("username"),
						prop.getProperty("password"));
			} else {
				Class.forName(prop.getProperty(dbname + ".driver"));
				c = DriverManager.getConnection(
						prop.getProperty(dbname + ".url"), 
						prop.getProperty(dbname + ".username"),
						prop.getProperty(dbname + ".password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(transaction)
			try { if(c.getAutoCommit() == true) c.setAutoCommit(false); } catch (SQLException e) { e.printStackTrace(); }
		
		return c;
	}
	
	public void commit() throws SQLException {
		conn.commit();
	}
	
	public void rollback() throws SQLException {
		conn.rollback();
	}
	
	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;

	public ResultSet executeQuery(String dbname, String sql)
			throws SQLException {
		conn = getConnection(dbname);
		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
		return rs;
	}

	public void close() {
		if(rs != null) try { rs.close(); rs = null; } catch (Exception e) {}
		if(stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
		if(conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
	}
	
	public void closeStatementAndResultSet() {
		if(rs != null) try { rs.close(); rs = null; } catch (Exception e) {}
		if(stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
	}

	String dbname;
	boolean transaction = false;
	
	public DBOperator() {
	}

	public DBOperator(String dbname) {
		this.dbname = dbname;
	}
	
	public DBOperator(String dbname, boolean transaction) {
		this.dbname = dbname;
		this.transaction = transaction;
	}

	/**
	 * 执行查询
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public ResultSet query(String sql) throws SQLException {
		// 取得数据库连接
		this.closeStatementAndResultSet();
		conn = getConnection(this.dbname);
		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
		return rs;
	}
	
	/**
	 * 执行带参数的查询操作
	 * 
	 * @param sql
	 * @param params
	 * @return
	 */
	public ResultSet query(String sql, Object[] params) throws SQLException {
		this.closeStatementAndResultSet();
		conn = getConnection(this.dbname);
		stmt = conn.prepareStatement(sql);
		sqlPstmtParamsType(stmt, params);
		rs = stmt.executeQuery();
		return rs;
	}

	/**
	 * 更新数据库
	 * 
	 * @param sql
	 * @param params
	 */
	public void update(String sql) throws SQLException {
		this.closeStatementAndResultSet();
		conn = getConnection(this.dbname);
		stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
	}

	/**
	 * 更新数据库
	 * 
	 * @param sql
	 * @param params
	 */
	public int update(String sql, Object[] params) throws SQLException {
		this.closeStatementAndResultSet();
		conn = getConnection(this.dbname);
		stmt = conn.prepareStatement(sql);
		sqlPstmtParamsType(stmt, params);
		int count = stmt.executeUpdate();

		return count;
	}
	
	/**
	 * 批量更新
	 * @param sql
	 * @param list
	 * @return
	 * @throws SQLException
	 */
	public int[] updateBatch(String sql, List<Object[]> list) throws SQLException {
		this.closeStatementAndResultSet();
		conn = getConnection(this.dbname);
		stmt = conn.prepareStatement(sql);
		for(Object[] vals : list) {
			for(int i = 0; i < vals.length; i++) {
				stmt.setObject(i + 1, vals[i]);
			}
			stmt.addBatch();
		}
		int counts[] = stmt.executeBatch();
		
		return counts;
	}
	
	/**
	 * 融合sql参数
	 * <pre>
	 * 注意该方法有缺陷
	 * </pre>
	 * @param pstmt
	 * @param params
	 * @throws PIPCountException
	 */
	private void sqlPstmtParamsType(PreparedStatement pstmt, Object[] params)
			throws SQLException {
		try {
			for (int i = 0; i < params.length; i++) {
				if (params[i] instanceof String) {
					pstmt.setString(i + 1, (String) params[i]);
				} else if (params[i] instanceof Integer) {
					pstmt.setInt(i + 1, ((Integer) params[i]).intValue());
				} else if (params[i] instanceof byte[]) {
					byte[] buf = (byte[]) params[i];
					pstmt.setBinaryStream(i + 1, new ByteArrayInputStream(buf),
							buf.length);
				} else if (params[i] instanceof Date) {
					pstmt.setDate(i + 1, new java.sql.Date(((Date) params[i]).getTime()));
				} else if (params[i] instanceof Double) {
					pstmt.setDouble(i + 1, ((Double) params[i]).doubleValue());
				} else if (params[i] instanceof Float) {
					pstmt.setFloat(i + 1, ((Float) params[i]).floatValue());
				}
			}
		} catch (SQLException e) {
			throw e;
		}
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public boolean isTransaction() {
		return transaction;
	}

	public void setTransaction(boolean transaction) {
		this.transaction = transaction;
	}

}
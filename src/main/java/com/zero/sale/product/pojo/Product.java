package com.zero.sale.product.pojo;

import java.util.Date;

public class Product {
    private Integer id;
    private String chineseName;
    private String englishName;
    private Double price;
    private Integer barnd;
    private Double weight;
    private Date updateDate;
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getChineseName() {
        return chineseName;
    }
    
    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }
    
    public String getEnglishName() {
        return englishName;
    }
    
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    
    public Double getPrice() {
        return price;
    }
    
    public void setPrice(Double price) {
        this.price = price;
    }
    
    public Integer getBarnd() {
        return barnd;
    }
    
    public void setBarnd(Integer barnd) {
        this.barnd = barnd;
    }

    
    public Double getWeight() {
        return weight;
    }

    
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    
    public Date getUpdateDate() {
        return updateDate;
    }

    
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
}

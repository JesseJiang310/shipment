package com.zero.sale.product.pojo;


public class Brand {
    public enum Country{
        NZ,
        AUSTRIALIA,
        AMERICAN,
        UK,
        TAILAND,
        GERMANY
    }
    private Integer id;
    private String englishName;
    private String chineseName;
    private Country country;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getEnglishName() {
        return englishName;
    }
    
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    
    public String getChineseName() {
        return chineseName;
    }
    
    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }
    
    public Country getCountry() {
        return country;
    }
    
    public void setCountry(Country country) {
        this.country = country;
    }
    
}

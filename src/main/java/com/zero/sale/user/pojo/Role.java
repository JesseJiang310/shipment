package com.zero.sale.user.pojo;

public class Role {

    /** 名称 */
    private String       name;

    /** 是否内置 */
    private Boolean      isSystem;

    /** 描述 */
    private String       description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Boolean isSystem) {
        this.isSystem = isSystem;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

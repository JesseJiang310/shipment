package com.zero.sale.user.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

import com.zero.framework.MyBatisSessionFactory;
import com.zero.sale.user.mapper.AdminMapper;
import com.zero.sale.user.pojo.Admin;

import com.zero.Principal;

@Component
public class AdminService {
    public Admin getCurrent() {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            Principal principal = (Principal) subject.getPrincipal();
            if (principal != null) {
                return getAdminById(principal.getId());
            }
        }
        return null;
    }
    
    public Admin getAdminById(Integer id) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            return mapper.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public Admin findByUsername(String username) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            Admin admin = mapper.findByUsername(username);
            return admin;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public void update(Admin admin) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            AdminMapper mapper = session.getMapper(AdminMapper.class);
            mapper.update(admin);
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
    public List<String> findAuthorities(Integer id) {
        List<String> authorities = new ArrayList<String>();
        Admin admin = getAdminById(id);
        if (admin != null) {
            SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
            try {
                AdminMapper mapper = session.getMapper(AdminMapper.class);
                authorities = mapper.getAuthorities(id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                session.close();
            }
        }
        return authorities;
    }
    public static void main(String[] args) {
        new AdminService().getAdminById(1);
    }
}

package com.zero.sale.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zero.sale.user.pojo.Admin;

public interface AdminMapper {

    public int deleteById(Integer id);
    public int update(Admin admin);
    public Admin getById(Integer id);
    public int insert(Admin admin);
    public Admin findByUsername(@Param(value="userName") String userName);
    public List<String> getAuthorities(Integer adminId);
}

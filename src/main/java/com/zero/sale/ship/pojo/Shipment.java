package com.zero.sale.ship.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

public class Shipment implements Serializable{
    public enum Company{
        FTD,
        EFS,
        ZTO,
        Flyway,
        VanGen
    }
    private Integer id;
    private String  code;
    private Integer status;
    private String  message;
    private Date    shipDate;
    private Date    arriveDate;
    private String consignee;
    private Company company;
    private Integer adminId;
    private Integer version;
    private Double nz;
    private Double cny;


    public Shipment(){
    }

    public Double getNz() {
        return nz;
    }

    public void setNz(Double nz) {
        this.nz = nz;
    }

    public Double getCny() {
        return cny;
    }

    public void setCny(Double cny) {
        this.cny = cny;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @DateTimeFormat(pattern="yyyy-MM-dd")
    public Date getShipDate() {
        return shipDate;
    }

    
    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    
    public Company getCompany() {
        return company;
    }

    
    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    
    public String getConsignee() {
        return consignee;
    }

    
    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    
    public Integer getAdminId() {
        return adminId;
    }

    
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public JSONArray getMessageJson(){
        if(!StringUtils.isEmpty(this.getMessage())){
            return JSON.parseArray(this.getMessage());
        }else {
            return new JSONArray();
        }
    }
}

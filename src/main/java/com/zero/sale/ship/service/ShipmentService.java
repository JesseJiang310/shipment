package com.zero.sale.ship.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.zero.Pageable;
import com.zero.sale.ship.plugin.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zero.framework.MyBatisSessionFactory;
import com.zero.sale.ship.mapper.ShipmentMapper;
import com.zero.sale.ship.pojo.Shipment;
import com.zero.sale.ship.pojo.Shipment.Company;
import com.zero.sale.user.pojo.Admin;
import com.zero.sale.user.service.AdminService;

@Component
public class ShipmentService {

    static Logger logger = Logger.getLogger(ShipmentService.class);
    @Autowired
    private AdminService adminService;
    public void updateShipment() {
        List<Shipment> shipments = getNoArriveList();
        for (Shipment shipment : shipments) {
            try {
                updateShipment(shipment.getId());
                Thread.sleep(new Random(System.currentTimeMillis()).nextInt(10)*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.error("update error:"+e.getMessage());
            }
        }
    }

    public void updateShipment(Integer id) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        ShipmentPlugin shipmentPlugin = null;
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            Shipment shipment = mapper.getById(id);
            List<String> traceResult = null;
            if (shipment.getCompany().equals(Company.FTD)) {
                shipmentPlugin = new FTDShipmentPlugin();
                traceResult = shipmentPlugin.trace(shipment.getCode());
            } else if (shipment.getCompany().equals(Company.EFS)) {
                shipmentPlugin = new EFSShipmentPlugin();
                traceResult = shipmentPlugin.trace(shipment.getCode());
            }else if (shipment.getCompany().equals(Company.ZTO)) {
                shipmentPlugin = new ZTOShipmentPlugin();
                traceResult = shipmentPlugin.trace(shipment.getCode());
            }else if (shipment.getCompany().equals(Company.Flyway)) {
                shipmentPlugin = new FlywayShipmentPlugin();
                traceResult = shipmentPlugin.trace(shipment.getCode());
            }else if (shipment.getCompany().equals(Company.VanGen)) {
                shipmentPlugin = new VanGenShipmentPlugin();
                traceResult = shipmentPlugin.trace(shipment.getCode());
            }
            if(traceResult.size()>0) {
                logger.info(traceResult.get(traceResult.size()-1));
            }
            JSONArray json = new JSONArray();
            json.addAll(traceResult);
            JSONArray jsonOld = JSON.parseArray(shipment.getMessage());
            if (!json.equals(jsonOld)) {
                shipment.setMessage(json.toJSONString());
            }
            if (traceResult.size() > 0 && traceResult.get(traceResult.size() - 1).contains("签收")) {
                shipment.setStatus(1);
                shipment.setArriveDate(new Date());
            }
            int result = mapper.update(shipment);
            if (result > 0) {
                logger.info("update " + shipment.getCode());
            } else {
                logger.error("update error " + shipment.getCode());
            }
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    public void delete(Integer id) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            mapper.deleteById(id);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
    public void update(Shipment shipment) {
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            mapper.update(shipment);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
    public List<Shipment> getNoArriveList() {
        List<Shipment> shipments = new ArrayList<Shipment>();
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            Admin admin = null;
            if(adminService!=null){
                admin=adminService.getCurrent();
            }
            if(admin!=null&&!"admin".equals(admin.getUserName())){
                shipments = mapper.getByStatus(0,adminService.getCurrent().getId(), 1000, null);
            } else {
                shipments = mapper.getByStatus(0,null, 1000, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return shipments;
    }
    public Shipment getShipmentById(Integer id){
        Shipment shipment = new Shipment();
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            shipment = mapper.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return shipment;
    }
    public List<Shipment> getLatestList(String company, Integer status, Pageable pageable) {
        List<Shipment> shipments = new ArrayList<Shipment>();
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            Admin admin =null;
            Integer adminId= null;
            if(adminService!=null){
                admin=adminService.getCurrent();
            }
            if(admin!=null&&!"admin".equals(admin.getUserName())){
                adminId= admin.getId();
            }
            shipments = mapper.getByDate(null, null,company,status,adminId, (pageable.getPageNumber()-1)*pageable.getPageSize(), pageable.getPageSize());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return shipments;
    }
    public void add(Shipment shipment){
        SqlSession session = MyBatisSessionFactory.getSqlSession("salenz");
        try {
            ShipmentMapper mapper = session.getMapper(ShipmentMapper.class);
            shipment.setVersion(0);
            shipment.setStatus(0);
            if(shipment.getShipDate()==null){
                shipment.setShipDate(new Date());
            }
            shipment.setAdminId(adminService.getCurrent().getId());
            mapper.insert(shipment);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
}

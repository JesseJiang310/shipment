package com.zero.sale.ship.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zero.sale.ship.service.ShipmentService;

@Component("shipJob")
@Lazy(false)
public class ShipJob {
    @Autowired
    private ShipmentService shipmentService;
    
    private Logger logger = Logger.getLogger(this.getClass());
    
    //@Scheduled(cron = "${job.updateShipment.cron}")
    public void updateShipment() {
        logger.info("start update shipment");
        try {
            shipmentService.updateShipment();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("job error:"+e.getMessage());
        }
        logger.info("end update shipment");

    }
    public static void main(String[] args) {
        ShipmentService shipmentService = new ShipmentService();
        shipmentService.updateShipment();
    }
}

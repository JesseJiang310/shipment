package com.zero.sale.ship.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zero.sale.ship.pojo.Shipment;

public interface ShipmentMapper {

    /**
     * @param status status 1 arrived 0 not arrived
     * @param start page start
     * @param size page size
     * @return
     */
    public List<Shipment> getByStatus(@Param(value = "status") Integer status,
                                      @Param(value = "adminId") Integer adminId, @Param(value = "start") Integer start,
                                      @Param(value = "size") Integer size);

    public int deleteById(Integer id);

    /**
     * @param shipment
     * @return
     */
    public int update(Shipment shipment);

    public Shipment getById(Integer id);

    public List<Shipment> getByDate(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
                                    @Param(value = "company") String company, @Param(value = "status") Integer status,
                                    @Param(value = "adminId") Integer adminId, @Param(value = "start") Integer start,
                                    @Param(value = "size") Integer size);

    public int insert(Shipment shipment);
}

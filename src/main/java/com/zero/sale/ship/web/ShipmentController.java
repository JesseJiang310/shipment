package com.zero.sale.ship.web;

import com.zero.Pageable;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zero.common.web.BaseController;
import com.zero.sale.ship.pojo.Shipment;
import com.zero.sale.ship.pojo.Shipment.Company;
import com.zero.sale.ship.service.ShipmentService;

import java.util.Date;

@Controller
@RequestMapping("/admin/ship")
public class ShipmentController extends BaseController {

    @Autowired
    private ShipmentService shipmentService;

    @RequestMapping("/list.jhtml")
    public String list(Model model, String company, Integer status, Pageable pageable) {
        model.addAttribute("list", shipmentService.getLatestList(company, status,pageable));
        model.addAttribute("companys",Shipment.Company.values());
        model.addAttribute("company", company);
        model.addAttribute("status", status);
        model.addAttribute("page", pageable);
        return "/admin/ship/list";
    }

    @RequestMapping("/track/{id}.jhtml")
    public String track(@PathVariable Integer id, Model model) {
        shipmentService.updateShipment(id);
        return "redirect:/admin/ship/list.jhtml";
    }


    @RequestMapping("/detail/{id}.jhtml")
    public String detail(@PathVariable Integer id, Model model) {
        model.addAttribute("ship", shipmentService.getShipmentById(id));
        return "/admin/ship/detail";
    }

    @RequestMapping("/delete/{id}.jhtml")
    public String delete(@PathVariable Integer id, Model model) {
        shipmentService.delete(id);
        return "redirect:/admin/ship/list.jhtml";
    }

    @RequestMapping("/arrive/{id}.jhtml")
    public String arrive(@PathVariable Integer id, Model model) {
        Shipment shipment = shipmentService.getShipmentById(id);
        shipment.setStatus(1);
        shipment.setArriveDate(new Date());
        shipmentService.update(shipment);
        return "redirect:/admin/ship/list.jhtml";
    }

    @RequestMapping("/add.jhtml")
    public String add(Model model) {
        model.addAttribute("companys", Company.values());
        return "/admin/ship/add";
    }

    @RequestMapping("/save.jhtml")
    public String save(Shipment shipment) {
        if (!StringUtils.isEmpty(shipment.getCode())) {
            shipment.setCode(shipment.getCode().trim().toUpperCase());
            shipmentService.add(shipment);
        }
        return "redirect:/admin/ship/list.jhtml";
    }
}

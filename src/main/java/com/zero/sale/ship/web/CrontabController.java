package com.zero.sale.ship.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zero.common.web.BaseController;
import com.zero.sale.ship.service.ShipmentService;

@Controller
@RequestMapping("/ship")
public class CrontabController extends BaseController {

    @Autowired
    private ShipmentService shipmentService;


    @RequestMapping("/track.jhtml")
    @ResponseBody public String track( Model model) {
        shipmentService.updateShipment();
        return "success";
    }
}

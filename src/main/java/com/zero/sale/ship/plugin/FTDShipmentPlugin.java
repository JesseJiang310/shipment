package com.zero.sale.ship.plugin;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FTDShipmentPlugin implements ShipmentPlugin {
    static Logger logger = Logger.getLogger(FTDShipmentPlugin.class);

    private final String url = "www.ftd.nz";

    public List<String> trace(String NO) {
        URI uri;
        try {
            Random random = new Random(Calendar.getInstance().getTimeInMillis());
            uri = new URIBuilder().setScheme("http").setHost(url).setPath("/query").build();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.setHeader("Referer", "http://www.ftd.nz/query/");
            httpPost.setHeader("User-Agent",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("codes", NO));
            formparams.add(new BasicNameValuePair("x", random.nextInt(100) + ""));
            formparams.add(new BasicNameValuePair("y", random.nextInt(100) + ""));
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
            httpPost.setEntity(httpEntity);
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                Header[] headers = response.getAllHeaders();
                return cleanHtml(EntityUtils.toString(entity), headers);
            }
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private List<String> cleanHtml(String html, Header[] headers) {
        List<String> result = new ArrayList<String>();
        Document doc = Jsoup.parse(html);
        Elements elements = doc.select(".qrBox").select(".qBox");
        if (elements != null && elements.size() > 1) {
            result.add(elements.get(0).html().replace(" ", ""));
        }
        Elements resultDiv = doc.select(".qrBox").select(".qRst");
        if (resultDiv != null) {
            Elements elementsResult = resultDiv.select("p");
            for (Element element : elementsResult) {
                if (element.html().contains("由于快递公司网络原因")) {
                    break;
                } else {
                    result.add(element.html().replace(" ", ""));
                }
            }
            Elements scripts = resultDiv.select("script");
            if (scripts != null && scripts.size() > 0) {
                try {
                    HttpGet httpGet = new HttpGet("http://" + url + scripts.attr("src"));
                    httpGet.setHeader("Referer", "http://www.ftd.nz/query/");
                    httpGet.setHeader("User-Agent",
                            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
                    RequestConfig.Builder builder = RequestConfig.custom();
                    builder.setCookieSpec(CookieSpecs.DEFAULT);
                    for (Header header : headers) {
                        if(header.getName().equalsIgnoreCase("Set-Cookie")){
                            httpGet.addHeader("Cookie", header.getValue());
                            break;
                        }
                    }
                    httpGet.setConfig(builder.build());
                    CloseableHttpClient httpclient = HttpClients.createDefault();
                    CloseableHttpResponse response = httpclient.execute(httpGet);
                    HttpEntity entity = response.getEntity();
                    Document docChinese = Jsoup.parse(EntityUtils.toString(entity, Charset.forName("utf-8")));
                    Elements p = docChinese.select("p");
                    if (p != null) {
                        for (Element element : p) {
                            result.add(element.html());
                        }
                    }
                    httpclient.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> list = new FTDShipmentPlugin().trace("NZ2890569");
        for (String ship :
                list) {
            System.out.println(ship);
        }
    }
}

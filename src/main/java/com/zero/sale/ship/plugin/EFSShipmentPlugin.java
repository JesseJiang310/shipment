package com.zero.sale.ship.plugin;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EFSShipmentPlugin implements ShipmentPlugin {
    static Logger logger = Logger.getLogger(EFSShipmentPlugin.class);

    private final String url  = "http://www.efspost.com/tool/track?s=%s";

    public List<String> trace(String NO) {
        try {
            HttpGet httpGet = new HttpGet(new URI(String.format(url,NO)));
            httpGet.setHeader("Referer", String.format(url, NO));
            httpGet.setHeader("User-Agent",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return  cleanHtml(EntityUtils.toString(entity));
            }
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private List<String> cleanHtml(String html) {
        List<String> result = new ArrayList<String>();
        Document doc = Jsoup.parse(html);
        Elements resultDiv = doc.select(".text-muted");
        if (resultDiv != null&&resultDiv.size()>0) {
            for (Element element : resultDiv) {
                Elements tds = element.select("td");
                if(tds!=null&&tds.size()==3){
                    result.add(tds.get(0).html()+" "+tds.get(2).html());
                }
            }
        }
        return Lists.reverse(result);
    }

    public static void main(String[] args) {
        List<String> result = new EFSShipmentPlugin().trace("EF516407519NZ");
        for (String string : result) {
            System.out.println(string);
        }
    }
}

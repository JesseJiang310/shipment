package com.zero.sale.ship.plugin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class VanGenShipmentPlugin implements ShipmentPlugin {
    static Logger logger = Logger.getLogger(VanGenShipmentPlugin.class);

    private final String url = "http://vangenexpress.co.nz/index.php?m=index&a=index";

    public List<String> trace(String NO) {
        try {
            HttpPost httpPost = new HttpPost(new URI(url));
            httpPost.setHeader("Referer", url);
            httpPost.setHeader("User-Agent",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("ids", NO));
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
            httpPost.setEntity(httpEntity);
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return cleanHtml(EntityUtils.toString(entity, Charset.forName("UTF-8")));
            }
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private List<String> cleanHtml(String html) {
        JSONArray jsonArray = JSONArray.parseArray(html);
        List<String> result = new ArrayList<String>();
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject= jsonArray.getJSONObject(i);
            result.add(jsonObject.getString("time")+ jsonObject.getString("context"));
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> list = new VanGenShipmentPlugin().trace("000000182791");
        for (String ship :
                list) {
            System.out.println(ship);
        }
    }
}

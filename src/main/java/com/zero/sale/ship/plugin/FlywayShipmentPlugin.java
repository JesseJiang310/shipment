package com.zero.sale.ship.plugin;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class FlywayShipmentPlugin implements ShipmentPlugin {
    static Logger logger = Logger.getLogger(FlywayShipmentPlugin.class);

    private final String url = "www.flywayex.com";

    public List<String> trace(String NO) {
        URI uri;
        try {
            Random random = new Random(Calendar.getInstance().getTimeInMillis());
            uri = new URIBuilder().setScheme("http").setHost(url).setPath("/cgi-bin/GInfo.dll?EmmisTrack").build();
            HttpPost httpPost = new HttpPost(new URI("http://www.flywayex.com/cgi-bin/GInfo.dll?EmmisTrack"));
            httpPost.setHeader("Referer", "http://www.flywayex.com/cgi-bin/GInfo.dll?EmmisTrack");
            httpPost.setHeader("User-Agent",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("cno", NO));
            formparams.add(new BasicNameValuePair("w", "flyway"));
            formparams.add(new BasicNameValuePair("cmodel",  ""));
            formparams.add(new BasicNameValuePair("ntype",  "0"));
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
            httpPost.setEntity(httpEntity);
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return cleanHtml(EntityUtils.toString(entity, Charset.forName("gb2312")));
            }
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private List<String> cleanHtml(String html) {
        List<String> result = new ArrayList<String>();
        Document doc = Jsoup.parse(html);
        Elements elements = doc.select("#oTHtable").select("tr");
        if (elements .size()>1) {
            for (int i = 1; i < elements.size(); i++) {
                Element tr = elements.get(i);
                Elements elementsResult = tr.select("td");
                if(elementsResult.size()==3){
                    result.add(elementsResult.get(0).html() + elementsResult.get(2).html());
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> list = new FlywayShipmentPlugin().trace("100001963009");
        for (String ship :
                list) {
            System.out.println(ship);
        }
    }
}

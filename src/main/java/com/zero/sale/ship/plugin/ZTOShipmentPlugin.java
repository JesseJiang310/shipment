package com.zero.sale.ship.plugin;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ZTOShipmentPlugin implements ShipmentPlugin {
    static Logger logger = Logger.getLogger(ZTOShipmentPlugin.class);

    private final String url  = "http://47.88.9.228/cgi-bin/GInfo.dll?EmmisTrack";

    public List<String> trace(String NO) {
        try {
            HttpPost httpPost = new HttpPost(new URI(url));
            httpPost.setHeader("Referer", url);
            httpPost.setHeader("User-Agent",
                               "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("cno", NO));
            formparams.add(new BasicNameValuePair("w", "nzztokd56"));
            formparams.add(new BasicNameValuePair("ntype", "0"));
            UrlEncodedFormEntity httpEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
            httpPost.setEntity(httpEntity);
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return  cleanHtml(EntityUtils.toString(entity));
            }
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    private List<String> cleanHtml(String html) {
        List<String> result = new ArrayList<String>();
        Document doc = Jsoup.parse(html);
        Elements resultDiv = doc.select(".trackContentTable").select("tr");
        if (resultDiv != null) {
            for (Element element : resultDiv) {
                if("center".equals(element.attr("align"))){
                    StringBuffer track = new StringBuffer();
                    for (Element e : element.select("td:nth-child(odd)")) {
                        track.append(e.html());
                    }
                    result.add(track.toString());
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        new ZTOShipmentPlugin().trace("64124350");
    }
}

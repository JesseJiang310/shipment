package com.zero.common.web;

import javax.servlet.ServletContext;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zero.sale.user.pojo.Admin;
import com.zero.sale.user.service.AdminService;



@Controller
@RequestMapping("/admin/common")
public class CommonController extends BaseController{
    @Autowired
    private AdminService adminService;
    /** servletContext */
    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    
    /**
     * 主页
     */
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String main(ModelMap model) {
        Admin admin = adminService.getCurrent();
        model.addAttribute("admin", admin);
        return "/admin/common/main";
    }
}

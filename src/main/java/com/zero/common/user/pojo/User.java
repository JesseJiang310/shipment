package com.zero.common.user.pojo;

import java.util.Date;

public class User {
    private Integer id;
    private String username;
    private String password;
    private Date loginTime;
    public User(Integer id, String username, String password){
        super();
        this.id = id;
        this.username = username;
        this.password = password;
    }
    public User(){
        super();
    }

    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Date getLoginTime() {
        return loginTime;
    }
    
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
    
}

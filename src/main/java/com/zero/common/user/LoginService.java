package com.zero.common.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import com.zero.common.user.pojo.User;
import com.zero.common.util.Const;
import com.zero.common.util.MD5;
import com.zero.common.util.Verify;

public class LoginService {
    private final String SSECRET_KEY="X(KfLu4(`R6zQq9Z";
    /**
     * 此方法的调用在前置验证都通过的前提下，所以只验证了用户名和密码
     */
    public void login(String username,String password){
        //验证用户名密码是否匹配
        User user = new User(1,"jiang","hao");
        if(password.equals(user.getPassword())){
            long time = System.currentTimeMillis();
            String key = user.getUsername() + user.getId() + SSECRET_KEY + time;
            String v = MD5.MD5_2(key).substring(16);
            String value = Verify.charCode(user.getUsername()) + Const.DELIM + user.getId()
                    + Const.DELIM + v + Const.DELIM + time;
            user.setLoginTime(new Date(time));
            try {
                value = URLEncoder.encode(value, Const.ENCODE.name());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {
        new LoginService().login("jiang", "hao");
    }
}

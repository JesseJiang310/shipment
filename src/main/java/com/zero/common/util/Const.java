package com.zero.common.util;

import java.nio.charset.Charset;

import org.apache.commons.codec.Charsets;


public class Const {
public static final String STRING_EMPUTY="";
public static final Charset ENCODE=Charsets.UTF_8;
public static final String DELIM="|";
public static final String COOKIEDOMAIN="zerocoffee.cn";
}
